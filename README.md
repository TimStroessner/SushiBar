# SushiBar

Bewerbungsprojekt für die Firma F&P von Tim Strößner

## Was wird benötigt?

Das Projekt ist in Angular CLI erstellt worden. Um es ausführen zu können muss auf dem Rechner Node.js installiert sein/werden (https://nodejs.org/en/).
Programmiert wurde es mit Typescript.

## Installation

* Repository Klonen oder Herunterladen
* Mit der Eingabeaufforderung in den Projektordner Navigieren (cd **\SushiBarOrdner)
* NPM installieren mit "npm install"
* Angular CLI installieren mit "npm install -g @angular/cli"
* Da ich Materialize verwendet habe, muss noch Angular Material installiert werden mit "npm install --save @angular/material @angular/cdk"

## Starten

Um einen einfachen dev-Server zu starten, kann man nun "ng serve --open" in die Eingabeaufforderung eingeben, und bei fertigstellung des Kompilierprozesses
öffnet sich Automatisch der Browser.

## Erklärung

Am anfang kann man einen Tisch mit beliebig vielen Stühlen anlegen. Anschließend kann die Anzahl der Gruppe festgelegt werden, welche anschließend vom Algorithmus automatisch
verteilt wird. Um den Sushi Meister größtmöglich zufrieden zu stellen, ist es auch möglich, Gruppen auf Nachfrage an nicht zusammenhängende Stühle verteilen zu lassen.

## Bearbeitete Dateien

Die Hauptdateien, in welchen ich Programmiert habe, befinden sich alle unter /src/app.

## Fragen?

Bei fragen stehe ich euch natürlich selbstverständlich jederzeit zur Verfügung.
