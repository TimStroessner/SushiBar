import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  chairCount: number = 1;
  disableForm = new FormControl(false);
  chairsHidden: boolean = true;
  freeChairs: number = 0;
  chairs: Chair[] = [];
  obtainedChairs: number = 0;
  groupSize: number = 0;
  groups: Group[] = [];
  groupsHidden: boolean = true;


  constructor() {}

  showChairs(){
    this.disableForm = new FormControl(true);
    this.chairsHidden = false;
    this.freeChairs = this.chairCount;
    this.generateChairChart();
  }

  generateChairChart(){
    let chair: Chair;
    for (let i = 0; i < this.chairCount; i++){
      chair = new Chair();
      chair.number = (i+1);
      chair.obtained = false;
      this.chairs.push(chair);
    }
  }

  distributeChairs(){
    this.findChairs();
    this.groupSize = 0;
  }

  findChairs(){
    let selectedFreeChairs: Chair[] = [];
    let selectedChairs: Chair[] = [];
    let chairCounter = 0;
    let chairCopy: Chair[] = [];
    let lastChairIndex: number = 0;
    let freeChairs: Chair[] = [];
    for(let chair of this.chairs){
      if(chair.obtained){
        lastChairIndex = this.chairs.indexOf(chair);
      }else{
        freeChairs.push(chair);
      }
    }
    console.log(lastChairIndex);
    chairCopy = this.chairs.slice(lastChairIndex);
    console.log(chairCopy);
    if(this.chairs.length >= this.groupSize){
      for(let count = 0; count <= 1; count++) {
        if(count === 1){
          chairCopy = this.chairs;
        }
        for(let chair of chairCopy){
          if(chairCounter < this.groupSize){
            if(chair.obtained){
              chairCounter = 0;
              selectedChairs = [];
            }else{
              chairCounter++;
              selectedChairs.push(chair);
            }
          }else{

          }
        }
      }
      if(selectedChairs.length < this.groupSize){
        if(freeChairs.length >= this.groupSize){
          if (confirm("Es sind nicht genügend aufeinanderfolgende Sitze verfügbar, jedoch besteht die Möglichkeit getrennt zu sitzen!")) {
            let ct = 0;
            for(let freeChair of freeChairs){
              if(ct < this.groupSize){
                selectedFreeChairs.push(freeChair);
                freeChair.obtained = true;
                ct++;
              }
            }
            this.freeChairs = this.freeChairs - this.groupSize;
            this.obtainedChairs = this.obtainedChairs + this.groupSize;
            let group = new Group();
            group.id = GUID.generateGUID();
            group.size = this.groupSize;
            group.seats = selectedFreeChairs;
            this.groups.push(group);
            this.groupsHidden = false;
            return true;
          } else {
          }
        }else{
          alert("Nicht genügend freie Plätze verfügbar!");
          return false;
        }
      }else{
        for(let selChair of selectedChairs){
          let foundChair = this.chairs.find(element => {
            return element.number == selChair.number;
          });
          foundChair.obtained = true;
        }
        this.freeChairs = this.freeChairs - this.groupSize;
        this.obtainedChairs = this.obtainedChairs + this.groupSize;
        let group = new Group();
        group.id = GUID.generateGUID();
        group.size = this.groupSize;
        group.seats = selectedChairs;
        this.groups.push(group);
        this.groupsHidden = false;
        return true;
      }
    }else{
      alert('Nicht genügend freie Plätze!');
      return false;
    }
  }

  deleteGroup(groupToDelete: Group){
    console.log(groupToDelete);
    for(let chair of this.chairs){
      for(let seat of groupToDelete.seats){
        if (chair.number === seat.number){
          chair.obtained = false;
          this.obtainedChairs = this.obtainedChairs - 1;
          this.freeChairs++;
        }
      }
    }
    let deleteGroup = this.groups.find(element => {
      return element.id == groupToDelete.id
    });
    let index = this.groups.indexOf(deleteGroup, 0);
    if (index > -1) {
      this.groups.splice(index, 1);
    }
  }

  confirmDelete(groupToDelete: Group) {
    if (confirm("Bitte bestätigen Sie das Entfernen der Gruppe")) {
      this.deleteGroup(groupToDelete);
    } else {
    }
  }
}

export class Group {
  id: String;
  size: Number;
  seats: Chair[];
}

export class Chair {
  number: Number;
  obtained: boolean;
}

export class GUID {
    private str:string;

    constructor(str?:string) {
        this.str = str || GUID.getNewGUIDString();
    }

    toString() {
        return this.str;
    }

    private static getNewGUIDString() {
        let d = new Date().getTime();
        if (window.performance && typeof window.performance.now === "function") {
            d += performance.now();
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    static generateGUID(){
        return ((new GUID()).toString().replace(/-/g, ''));
    }
}


